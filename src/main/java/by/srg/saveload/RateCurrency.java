package by.srg.saveload;

import by.srg.model.Currency;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RateCurrency {

    public static HashMap<String, Double> getRates(Currency base) throws Exception {

        HashMap<String, NodeList> result = new HashMap<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String url = "https://www.nbrb.by/services/xmlexrates.aspx?ondate=" + dateFormat.format(new Date());

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document doc = factory.newDocumentBuilder().parse(new URL(url).openStream());

        NodeList nl = doc.getElementsByTagName("Currency");
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            NodeList nlChild = node.getChildNodes();
            for (int j = 0; j < nlChild.getLength(); j++) {
                if (nlChild.item(j).getNodeName().equals("CharCode")) {
                    result.put(nlChild.item(j).getTextContent(), nlChild);
                }
            }
        }

        HashMap<String, Double> rates = new HashMap<>();
        for (Map.Entry<String, NodeList> entry : result.entrySet()) {
            NodeList temp = entry.getValue();
            double value = 0;
            int nominal = 0;
            for (int i = 0; i < temp.getLength(); i++) {
                if (temp.item(i).getNodeName().equals("Rate")) {
                    value = Double.parseDouble(temp.item(i).getTextContent().replace(",", "."));
                } else if (temp.item(i).getNodeName().equals("Scale")) {
                    nominal = Integer.parseInt(temp.item(i).getTextContent());
                }
            }
            double amount = value / nominal;
            rates.put(entry.getKey(), (((double) Math.round(amount * 10000)) / 10000));
        }

        rates.put("BYN", 1.0);
        double div = rates.get(base.getCode());
        for (Map.Entry<String, Double> entry : rates.entrySet()) {
            entry.setValue(entry.getValue() / div);
        }
        return rates;
    }
}
