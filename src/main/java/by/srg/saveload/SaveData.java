package by.srg.saveload;

import by.srg.exception.ModelException;
import by.srg.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public final class SaveData {

    private static SaveData instance;
    private List<Article> articles = new ArrayList<>();
    private List<Currency> currencies = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();
    private List<Transfer> transfers = new ArrayList<>();

    private final Filter filter;
    private Common oldCommon;
    private boolean saved = true;

    private SaveData() {
        load();
        this.filter = new Filter();
    }

    public void load() {
        SaveLoad.load(this);
        sort();
        for (Account a : accounts) {
            a.setAmountFromTransactionsAndTransfers(transactions, transfers);
        }
    }

    public void clear() {
        articles.clear();
        currencies.clear();
        accounts.clear();
        transactions.clear();
        transfers.clear();
    }

    private void sort() {
        this.articles.sort((Article a, Article a1) -> a.getTitle().compareToIgnoreCase(a1.getTitle()));
        this.accounts.sort((Account a, Account a1) -> a.getTitle().compareToIgnoreCase(a1.getTitle()));
        this.transactions.sort((Transaction t, Transaction t1) -> (int) (t1.getDate().compareTo(t.getDate())));
        this.transfers.sort((Transfer t, Transfer t1) -> (int) (t1.getDate().compareTo(t.getDate())));
        this.currencies.sort(new Comparator<Currency>() {
            @Override
            public int compare(Currency c, Currency c1) {
                if (c.isBase()) return -1;
                if (c1.isBase()) return 1;
                if (c.isOn() ^ c1.isOn()) {  //Проверить
                    if (c1.isOn()) return 1;
                    else return -1;
                }
                return c.getTitle().compareToIgnoreCase(c1.getTitle());
            }
        });
    }

    public void save() {
        SaveLoad.save(this);
        saved = true;
    }

    public boolean isSaved() {
        return saved;
    }

    public static SaveData getInstance() {
        if (instance == null) {
            instance = new SaveData();
        }
        return instance;
    }

    public Filter getFilter() {
        return filter;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setArticles(List<Article> articles) {
        if (articles != null) this.articles = articles;
    }

    public void setCurrencies(List<Currency> currencies) {
        if (currencies != null) this.currencies = currencies;
    }

    public void setAccounts(List<Account> accounts) {
        if (accounts != null) this.accounts = accounts;
    }

    public void setTransactions(List<Transaction> transactions) {
        if (transactions != null) this.transactions = transactions;
    }

    public void setTransfers(List<Transfer> transfers) {
        if (transfers != null) this.transfers = transfers;
    }

    public Currency getBaseCurrency() {
        for (Currency currency : currencies) {
            if (currency.isBase()) return currency;
        }
        return new Currency();
    }

    public List<Currency> getEnableCurrencies() {
        List<Currency> list = new ArrayList<>();
        for (Currency currency : currencies) {
            if (currency.isOn()) list.add(currency);
        }
        return list;
    }

    public List<Transaction> getFilterTransactions() {
        List<Transaction> list = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (filter.check(transaction.getDate())) list.add(transaction);
        }
        return list;
    }

    public List<Transfer> getFilterTransfers() {
        List<Transfer> list = new ArrayList<>();
        for (Transfer transfer : transfers) {
            if (filter.check(transfer.getDate())) list.add(transfer);
        }
        return list;
    }

    public List<Transaction> getTransactionOnCount(int count) {
        return new ArrayList<>(transactions.subList(0, Math.min(count, transactions.size())));
    }

    public Common getOldCommon() {
        return oldCommon;
    }

    public void add(Common common) throws ModelException {
        List ref = getRef(common);
        if (ref != null && ref.contains(common)) throw new ModelException(ModelException.IS_EXISTS);
        ref.add(common);
        common.postAdd(this);
        sort();
        saved = false;
    }

    public void edit(Common oldCommon, Common newCommon) throws ModelException {
        List ref = getRef(oldCommon);
        if (ref.contains(newCommon) && oldCommon != ref.get(ref.indexOf(newCommon)))
            throw new ModelException(ModelException.IS_EXISTS);
        ref.set(ref.indexOf(oldCommon), newCommon);
        this.oldCommon = oldCommon;
        newCommon.postEdit(this);
        sort();
        saved = false;
    }

    public void remove(Common common) {
        getRef(common).remove(common);
        common.postRemove(this);
        saved = false;
    }

    @Override
    public String toString() {
        return "SaveData{" +
                "articles=" + articles +
                ", currencies=" + currencies +
                ", accounts=" + accounts +
                ", transactions=" + transactions +
                ", transfers=" + transfers +
                '}';
    }

    private List getRef(Common common) {
        if (common instanceof Account) return accounts;
        else if (common instanceof Article) return articles;
        else if (common instanceof Currency) return currencies;
        else if (common instanceof Transaction) return transactions;
        else if (common instanceof Transfer) return transfers;
        return null;
    }

    public void updateCurrencies() throws Exception {
        HashMap<String, Double> rates = RateCurrency.getRates(getBaseCurrency());
        for (Currency currency : currencies) {
            currency.setRate(rates.get(currency.getCode()));
        }
        for (Account account : accounts) {
            account.getCurrency().setRate(rates.get(account.getCurrency().getCode()));
        }
        saved = false;
    }
}
