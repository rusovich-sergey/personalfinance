package by.srg.gui.table;

import by.srg.gui.Refresh;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.menu.TablePopupMenu;
import by.srg.gui.table.model.MainTableModel;
import by.srg.gui.table.renderer.MainTableCellRender;
import by.srg.gui.table.renderer.TableHeaderIconRenderer;
import by.srg.settings.Style;
import by.srg.settings.Text;

import javax.swing.*;
import java.awt.*;

abstract public class TableData extends JTable implements Refresh {

    private final FunctionsHandler handler;
    private final TablePopupMenu popupMenu;
    private final String[] columns;
    private final ImageIcon[] icons;

    public TableData(MainTableModel model, FunctionsHandler handler, String[] columns, ImageIcon[] icons) {
        super(model);
        this.handler = handler;
        this.popupMenu = new TablePopupMenu(handler);
        this.columns = columns;
        this.icons = icons;

        getTableHeader().setFont(Style.FONT_TABLE_HEADER);
        setFont(Style.FONT_TABLE);
        setRowHeight(getRowHeight() + Style.TABLE_ADD_ROW_HEIGHT);

        setAutoCreateRowSorter(true);
        setPreferredScrollableViewportSize(Style.DIMENSION_TABLE_SHOW_SIZE);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        addMouseListener(handler);
        addKeyListener(handler);

        for (int i = 0; i < columns.length; i++) {
            getColumn(Text.get(columns[i])).setHeaderRenderer(new TableHeaderIconRenderer(icons[i]));
        }

        MainTableCellRender renderer = new MainTableCellRender();
        setDefaultRenderer(String.class, renderer);
        setComponentPopupMenu(popupMenu);
    }

    @Override
    public JPopupMenu getComponentPopupMenu() {
        Point point = getMousePosition();
        if (point != null) {
            int row = rowAtPoint(point);
            if (isRowSelected(row)) return super.getComponentPopupMenu();
            else return null;
        }
        return super.getComponentPopupMenu();
    }

    @Override
    public void refresh() {
        int selectedRow = getSelectedRow();
        ((MainTableModel) getModel()).refresh();
        for (int i = 0; i < columns.length; i++) {
            getColumn(Text.get(columns[i])).setHeaderRenderer(new TableHeaderIconRenderer(icons[i]));
        }
        if (selectedRow != -1 && selectedRow < getRowCount()) {
            setRowSelectionInterval(selectedRow, selectedRow);
            requestFocus();
        }
        init();
    }

    protected void init() {
    }

    public FunctionsHandler getFunctionHandler() {
        return handler;
    }
}
