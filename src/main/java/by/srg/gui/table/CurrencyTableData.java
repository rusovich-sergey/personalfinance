package by.srg.gui.table;

import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.model.CurrencyTableModel;
import by.srg.gui.table.model.MainTableModel;
import by.srg.gui.table.renderer.MainTableCellRender;
import by.srg.model.Currency;
import by.srg.settings.Style;
import by.srg.settings.Text;

import javax.swing.*;
import java.awt.*;

public class CurrencyTableData extends TableData {

    private static String[] columns = new String[]{"TITLE", "CODE", "RATE", "ON", "BASE"};
    private static final ImageIcon[] icons = new ImageIcon[]{Style.ICON_TITLE, Style.ICON_CODE,
            Style.ICON_RATE, Style.ICON_ON, Style.ICON_BASE};

    public CurrencyTableData(FunctionsHandler handler) {
        super(new CurrencyTableModel(columns), handler, columns, icons);
        init();
    }

    @Override
    protected final void init() {
        for (String column : columns) {
            getColumn(Text.get(column)).setCellRenderer(new TableCellOnOfRenderer());
        }
    }

    private class TableCellOnOfRenderer extends MainTableCellRender {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (((Currency) ((MainTableModel) table.getModel()).getCommonByRow(row)).isOn()) {
                renderer.setForeground(Style.COLOR_ON);
            } else renderer.setForeground(Style.COLOR_OFF);
            return renderer;
        }
    }
}
