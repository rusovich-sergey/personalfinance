package by.srg.gui.table.model;

import by.srg.gui.Refresh;
import by.srg.model.Common;
import by.srg.settings.Text;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

abstract public class MainTableModel extends AbstractTableModel implements Refresh {

    protected List<? extends Common> data;
    protected List<String> columns = new ArrayList<>();

    public MainTableModel(List data, String[] columns) {
        this.data = data;
        this.columns = new ArrayList<>(Arrays.asList(columns));
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public String getColumnName(int column) {
        return Text.get(columns.get(column));
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Object o = getValueAt(0, columnIndex);
        if (o == null) return Object.class;
        return o.getClass();
    }

    @Override
    public void refresh() {
        updateDate();
        fireTableStructureChanged();
        updateDate();
    }

    public Common getCommonByRow(int row) {
        return data.get(row);
    }

    abstract protected void updateDate();
}
