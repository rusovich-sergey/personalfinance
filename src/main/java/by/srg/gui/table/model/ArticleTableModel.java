package by.srg.gui.table.model;

import by.srg.model.Article;
import by.srg.saveload.SaveData;

public class ArticleTableModel extends MainTableModel {

    private static final int TITLE = 0;

    public ArticleTableModel(String[] columns) {
        super(SaveData.getInstance().getArticles(), columns);
    }

    @Override
    protected void updateDate() {
        data = SaveData.getInstance().getArticles();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (data.isEmpty()) return null;
        Article article = (Article) data.get(rowIndex);
        switch (columnIndex) {
            case TITLE:
                return article.getTitle();
        }
        return null;
    }
}
