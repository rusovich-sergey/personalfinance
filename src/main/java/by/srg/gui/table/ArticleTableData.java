package by.srg.gui.table;

import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.model.ArticleTableModel;
import by.srg.settings.Style;

import javax.swing.*;

public class ArticleTableData extends TableData {

    private static String[] columns = new String[]{"TITLE"};
    private static final ImageIcon[] icons = new ImageIcon[]{Style.ICON_TITLE};

    public ArticleTableData(FunctionsHandler handler) {
        super(new ArticleTableModel(columns), handler, columns, icons);
    }
}
