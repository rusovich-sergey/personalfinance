package by.srg.gui.handler;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.AbstractAddEditDialog;
import by.srg.gui.dialog.ConfirmDialog;
import by.srg.gui.table.TableData;
import by.srg.gui.table.model.MainTableModel;
import by.srg.model.Common;
import by.srg.saveload.SaveData;
import by.srg.settings.HandlerCode;

import javax.swing.*;
import java.awt.event.*;

public class FunctionsHandler extends Handler implements MouseListener, KeyListener {

    private final AbstractAddEditDialog dialog;

    public FunctionsHandler(MainFrame frame, AbstractAddEditDialog dialog) {
        super(frame);
        this.dialog = dialog;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        switch (ae.getActionCommand()) {
            case HandlerCode.ADD:
                add();
                break;
            case HandlerCode.EDIT:
                edit();
                break;
            case HandlerCode.DELETE:
                delete();
                break;
        }
        super.actionPerformed(ae);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof TableData) {
            if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
                showAddEditDialog(getSelectedCommon());
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) delete();
        frame.refresh();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() instanceof TableData) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                TableData td = frame.getRightPanel().getTableDate();
                int row = td.rowAtPoint(e.getPoint());
                td.setRowSelectionInterval(row, row);
            }
        }
    }

    public void add() {
        showAddEditDialog(null);
    }

    public void edit() {
        showAddEditDialog(getSelectedCommon());
    }

    public void delete() {
        Common c = getSelectedCommon();
        if (c != null) {
            int result = ConfirmDialog.show(frame, "CONFIRM_DELETE_TEXT", "CONFIRM_DELETE_TITLE");
            if (result == JOptionPane.YES_OPTION) {
                SaveData.getInstance().remove(c);
            }
        }
    }

    private Common getSelectedCommon() {
        TableData td = frame.getRightPanel().getTableDate();
        return ((MainTableModel) td.getModel()).getCommonByRow(td.getSelectedRow());
    }

    private void showAddEditDialog(Common c) {
        dialog.setCommon(c);
        dialog.showDialog();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
