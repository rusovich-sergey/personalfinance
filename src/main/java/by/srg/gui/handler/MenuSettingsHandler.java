package by.srg.gui.handler;

import by.srg.gui.MainFrame;
import by.srg.settings.HandlerCode;
import by.srg.settings.Settings;
import by.srg.settings.Text;

import java.awt.event.ActionEvent;

public class MenuSettingsHandler extends Handler {

    public MenuSettingsHandler(MainFrame frame) {
        super(frame);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        switch (ae.getActionCommand()) {
            case HandlerCode.MENU_SETTINGS_LANGUAGE_RUSSIAN: {
                Settings.setLanguage("ru");
                break;
            }
            case HandlerCode.MENU_SETTINGS_LANGUAGE_ENGLISH: {
                Settings.setLanguage("en");
                break;
            }
        }
        Text.init();
        frame.getMenu().refresh();
        super.actionPerformed(ae);
    }
}
