package by.srg.gui.handler;

import by.srg.gui.MainFrame;
import by.srg.saveload.SaveData;
import by.srg.settings.HandlerCode;

import java.awt.event.ActionEvent;

public class FilterHandler extends MenuViewHandler {

    public FilterHandler(MainFrame frame) {
        super(frame);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        switch (ae.getActionCommand()) {
            case HandlerCode.LEFT: {
                SaveData.getInstance().getFilter().prev();
                break;
            }
            case HandlerCode.RIGHT: {
                SaveData.getInstance().getFilter().next();
                break;
            }
            case HandlerCode.STEP: {
                SaveData.getInstance().getFilter().nextPeriod();
                break;
            }
        }
        super.actionPerformed(ae);
    }
}
