package by.srg.gui.handler;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.AboutDialog;
import by.srg.settings.HandlerCode;

import java.awt.event.ActionEvent;

public class MenuHelpHandler extends Handler {

    public MenuHelpHandler(MainFrame frame) {
        super(frame);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        switch (ae.getActionCommand()) {
            case HandlerCode.MENU_HELP_ABOUT: {
                new AboutDialog().setVisible(true);
                break;
            }
        }
    }
}
