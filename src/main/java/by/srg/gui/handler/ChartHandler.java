package by.srg.gui.handler;

import by.srg.gui.MainFrame;
import by.srg.gui.panel.StatisticsPanel;
import by.srg.settings.HandlerCode;

import java.awt.event.ActionEvent;

public class ChartHandler extends MenuViewHandler {

    public ChartHandler(MainFrame frame) {
        super(frame);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        switch (ae.getActionCommand()) {
            case HandlerCode.TYPE: {
                ((StatisticsPanel) frame.getRightPanel()).nextType();
                break;
            }
        }
        super.actionPerformed(ae);
    }
}