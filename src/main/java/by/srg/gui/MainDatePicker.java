package by.srg.gui;

import by.srg.settings.Style;
import by.srg.settings.Text;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.swing.JButton;
import java.util.Date;
import java.util.Properties;

public class MainDatePicker {

    private final JDatePickerImpl picker;

    public MainDatePicker() {
        UtilDateModel model = new UtilDateModel();
        Properties properties = new Properties();
        properties.put("text.today", Text.get("TODAY"));
        JDatePanelImpl panel = new JDatePanelImpl(model, properties);

        picker = new JDatePickerImpl(panel, new DateComponentFormatter());
        model.setValue(new Date());

        JButton button = (JButton) picker.getComponent(1);
        button.setIcon(Style.ICON_DATE);
        button.setText("");
    }

    public JDatePickerImpl getPicker () {
        return picker;
    }

    public void setValue(Date date) {
        ((UtilDateModel) picker.getModel()).setValue(date);
    }
}
