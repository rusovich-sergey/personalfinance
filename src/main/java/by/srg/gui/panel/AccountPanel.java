package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.AccountAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.AccountTableData;
import by.srg.gui.toolbar.FunctionToolBar;
import by.srg.settings.Style;

public class AccountPanel extends RightPanel {

    public AccountPanel(MainFrame frame) {
        super(
                frame,
                new AccountTableData(new FunctionsHandler(frame, new AccountAddEditDialog(frame))),
                "ACCOUNTS",
                Style.ICON_PANEL_ACCOUNTS, new FunctionToolBar(new FunctionsHandler(frame, new AccountAddEditDialog(frame)))
        );
    }
}
