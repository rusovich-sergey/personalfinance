package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.TransactionAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.TransactionTableData;
import by.srg.settings.Settings;
import by.srg.settings.Style;

public class OverviewPanel extends RightPanel {

    public OverviewPanel(MainFrame frame) {
        super(
                frame,
                new TransactionTableData(new FunctionsHandler(frame, new TransactionAddEditDialog(frame)), Settings.COUNT_OVERVIEW_ROWS),
                "LAST_TRANSACTIONS",
                Style.ICON_PANEL_OVERVIEW
        );
    }
}
