package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.TransferAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.TransferTableData;
import by.srg.gui.toolbar.FunctionToolBar;
import by.srg.settings.Style;

import javax.swing.*;

public class TransferPanel extends RightPanel {

    public TransferPanel(MainFrame frame) {
        super(
                frame,
                new TransferTableData(new FunctionsHandler(frame, new TransferAddEditDialog(frame))),
                "TRANSFERS",
                Style.ICON_PANEL_TRANSFERS,
                new JPanel[]{new FunctionToolBar(new FunctionsHandler(frame, new TransferAddEditDialog(frame))), new FilterPanel(frame)}
        );
    }
}
