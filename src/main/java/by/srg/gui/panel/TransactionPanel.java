package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.TransactionAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.TransactionTableData;
import by.srg.gui.toolbar.FunctionToolBar;
import by.srg.settings.Style;

import javax.swing.*;

public class TransactionPanel extends RightPanel {

    public TransactionPanel(MainFrame frame) {
        super(
                frame,
                new TransactionTableData(new FunctionsHandler(frame, new TransactionAddEditDialog(frame))),
                "TRANSACTIONS",
                Style.ICON_PANEL_TRANSACTIONS,
                new JPanel[]{new FunctionToolBar(new FunctionsHandler(frame, new TransactionAddEditDialog(frame))), new FilterPanel(frame)}
        );
    }
}
