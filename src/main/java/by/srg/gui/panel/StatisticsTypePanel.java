package by.srg.gui.panel;

import by.srg.gui.MainButton;
import by.srg.gui.MainFrame;
import by.srg.gui.handler.ChartHandler;
import by.srg.settings.HandlerCode;
import by.srg.settings.Text;

public class StatisticsTypePanel extends AbstractPanel {

    private final String title;

    public StatisticsTypePanel(MainFrame frame, String title) {
        super(frame);
        this.title = Text.get(title);
        init();
    }

    @Override
    protected void init() {
        MainButton button = new MainButton(title, new ChartHandler(frame), HandlerCode.TYPE);
        add(button);
    }
}
