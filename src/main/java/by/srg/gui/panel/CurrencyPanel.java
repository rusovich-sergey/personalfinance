package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.CurrencyAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.CurrencyTableData;
import by.srg.gui.toolbar.FunctionToolBar;
import by.srg.settings.Style;

public class CurrencyPanel extends RightPanel {

    public CurrencyPanel(MainFrame frame) {
        super(
                frame,
                new CurrencyTableData(new FunctionsHandler(frame, new CurrencyAddEditDialog(frame))),
                "CURRENCIES",
                Style.ICON_PANEL_CURRENCIES, new FunctionToolBar(new FunctionsHandler(frame, new CurrencyAddEditDialog(frame)))
        );
    }
}
