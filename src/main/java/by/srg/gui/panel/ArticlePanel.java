package by.srg.gui.panel;

import by.srg.gui.MainFrame;
import by.srg.gui.dialog.ArticleAddEditDialog;
import by.srg.gui.handler.FunctionsHandler;
import by.srg.gui.table.ArticleTableData;
import by.srg.gui.toolbar.FunctionToolBar;
import by.srg.settings.Style;

public class ArticlePanel extends RightPanel {

    public ArticlePanel(MainFrame frame) {
        super(
                frame,
                new ArticleTableData(new FunctionsHandler(frame, new ArticleAddEditDialog(frame))),
                "ARTICLES",
                Style.ICON_PANEL_ARTICLES, new FunctionToolBar(new FunctionsHandler(frame, new ArticleAddEditDialog(frame)))
        );
    }
}
