package by.srg.gui.toolbar;

import by.srg.gui.EnableEditDelete;
import by.srg.gui.MainButton;
import by.srg.gui.handler.Handler;
import by.srg.settings.HandlerCode;
import by.srg.settings.Style;
import by.srg.settings.Text;

public class FunctionToolBar extends AbstractToolBar implements EnableEditDelete {

    private MainButton editButton;
    private MainButton deleteButton;

    public FunctionToolBar(Handler handler) {
        super(Style.BORDER_FUNCTION_TOOLBAR, handler);
        init();
    }

    @Override
    protected void init() {
        addButton(Text.get("ADD"), Style.ICON_ADD, HandlerCode.ADD, false);
        editButton = addButton(Text.get("EDIT"), Style.ICON_EDIT, HandlerCode.EDIT, false);
        deleteButton = addButton(Text.get("DELETE"), Style.ICON_DELETE, HandlerCode.DELETE, false);
    }

    @Override
    public void setEnableEditDelete(boolean enable) {
        editButton.setEnabled(enable);
        deleteButton.setEnabled(enable);
    }
}
