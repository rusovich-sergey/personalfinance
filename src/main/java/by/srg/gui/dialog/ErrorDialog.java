package by.srg.gui.dialog;

import by.srg.gui.MainFrame;
import by.srg.settings.Text;

import javax.swing.*;

public class ErrorDialog {
    public static void show (MainFrame frame, String text) {
        JOptionPane.showMessageDialog(frame, Text.get(text), Text.get("ERROR"), JOptionPane.ERROR_MESSAGE);
    }
}
