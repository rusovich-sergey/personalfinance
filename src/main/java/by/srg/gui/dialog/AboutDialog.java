package by.srg.gui.dialog;

import by.srg.settings.Style;
import by.srg.settings.Text;
import javafx.scene.control.Hyperlink;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class AboutDialog extends JDialog {

    public AboutDialog() {
        super();
        init();
        setTitle(Text.get("DIALOG_ABOUT_TITLE"));
        setIconImage(Style.ICON_MENU_HELP_ABOUT.getImage());
        setResizable(false);
    }

    public void init() {
        JEditorPane pane = new JEditorPane("text/html", Text.get("ABOUT"));
        pane.setEditable(false);
        pane.setOpaque(false);

        pane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent hl) {
                if(HyperlinkEvent.EventType.ACTIVATED.equals(hl.getEventType())) {
                    try {
                        Desktop.getDesktop().browse(hl.getURL().toURI());
                    } catch (IOException | URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        add(pane);
        pack();
        setLocationRelativeTo(null);
    }
}
