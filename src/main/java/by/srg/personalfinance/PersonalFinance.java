package by.srg.personalfinance;

import by.srg.exception.ModelException;
import by.srg.gui.MainFrame;
import by.srg.model.*;
import by.srg.saveload.SaveData;
import by.srg.settings.Settings;
import by.srg.settings.Text;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersonalFinance {
    public static void main(String[] args) throws Exception {
        init();
        SaveData saveData = SaveData.getInstance();

        MainFrame frame = new MainFrame();
        frame.setVisible(true);

//        testModel();
    }

    private static void init() {
        Settings.init();
        Text.init();
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        try {
            graphicsEnvironment.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("fonts/Roboto_Light.ttf")));
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();  //Logger
        }
    }
}
